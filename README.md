# Todo app built with Django, HTMX, and Tailwind CSS

I built this for the sole purpose of learning htmx and Tailwind CSS.
With regard to htmx in particular, I'm trying to figure out if it could
replace frameworks like Angular for some of my projects.

Currently, the only htmx feature I'm using is `hx-boost` on `<body>`.

- Django - https://www.djangoproject.com/
- Django REST Framework - https://www.django-rest-framework.org/
- htmx - https://htmx.org/
- Tailwind CSS - https://tailwindcss.com/

## Installation

    pipx install poetry
    git clone git@bitbucket.org:wyatt/yata.git
    cd yata
    poetry install
    npm install
    # activate the virtualenv created by poetry
    run django migrate
    run dev-server
    # open another terminal tab/window
    run compile-css
