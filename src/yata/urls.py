from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework.routers import SimpleRouter

from .views.todos import TodoViewSet

router = SimpleRouter()
router.register("todos", TodoViewSet)

urlpatterns = [path("admin/", admin.site.urls), path("", include(router.urls))]

if settings.DEBUG:
    urlpatterns.append(path("__debug__/", include("debug_toolbar.urls")))
