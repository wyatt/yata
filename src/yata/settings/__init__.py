from django.core.management import utils
from local_settings import EnvSetting, SecretSetting, inject_settings

DEBUG = False
SECRET_KEY = SecretSetting(default=utils.get_random_secret_key)

inject_settings()
