from rest_framework import serializers
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.viewsets import ModelViewSet

from .. import models


class TemplateHTMLRenderer(TemplateHTMLRenderer):
    def get_template_context(self, data, render_context):
        view = render_context["view"]
        match view.action:
            case "list" | "destroy":
                name = "todos"
            case _:
                name = "todo"
        context = super().get_template_context(data, render_context)
        context = {name: context}
        return context


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Todo
        fields = ("id", "title", "completed", "created")


class TodoViewSet(ModelViewSet):
    queryset = models.Todo.objects.order_by("completed", "created").all()
    renderer_classes = [TemplateHTMLRenderer]
    serializer_class = TodoSerializer

    def destroy(self, request, *args, **kwargs):
        super().destroy(request, *args, **kwargs)
        return self.list(request)

    def get_template_names(self):
        self.response.data = {"result": self.response.data}
        match self.action:
            case "list":
                template_name = "todos/list.html"
            case "destroy":
                template_name = "todos/items.html"
            case _:
                template_name = "todos/item.html"
        return [template_name]
