import os
import shlex

from runcommands import command
from runcommands import commands as c
from runcommands import printer

PROJECT_DIR = os.path.dirname(__file__)


@command
def install(update=False):
    if update:
        c.local("poetry update")
    c.local("poetry install")


@command
def format_():
    poetry_run("black .")
    poetry_run("isort --profile black .")


@command
def type_check(incremental=True, raise_on_error=True):
    """Type check code with mypy."""
    printer.hr("Type checking with Mypy")
    return c.local(
        ("mypy", "--incremental" if incremental else "--no-incremental"),
        raise_on_error=raise_on_error,
    )


# Static files


@command
def compile_css():
    printer.header("Compiling CSS and watching for changes...")
    c.local("postcss -w src/yata/static/main.tailwind.css -o src/yata/static/main.css")


# Django ---------------------------------------------------------------


@command
def dev_server(env="dev"):
    """Run the Django dev server."""
    django("runserver", env=env)


@command
def urls(env="dev"):
    """Show Django URLs."""
    from django.urls import get_resolver

    set_django_env_vars(env, True)
    patterns = get_resolver().url_patterns
    for pattern in patterns:
        print(pattern)


@command
def django(*args, env="dev"):
    """Run a Django management command."""
    from django.core.management import execute_from_command_line

    set_django_env_vars(env)
    execute_from_command_line(["run django"] + list(args))


def set_django_env_vars(env, setup=False):
    import django

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "yata.settings")
    os.environ.setdefault("LOCAL_SETTINGS_FILE", f"yata.settings:{env}.cfg")

    if setup:
        django.setup()


# Utilities ------------------------------------------------------------


def poetry_run(args):
    if isinstance(args, str):
        args = shlex.split(args)
    printer.print("poetry", "run", " ".join(args))
    c.local(("poetry", "run", args), cd=PROJECT_DIR)
